import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Random;

public class HopelessArchive {
    private final Path readPath;
    private Integer cursorIndex = null; // store as byte, will be <255
    private Integer seed = null;
    private Byte mod = null;
    private Byte offset = null;
    final int runSize = 8;

    public HopelessArchive(Path readPath) throws IOException {
        this.readPath = readPath;
        boolean runFound = findRun();
    }

    private boolean findRun() throws IOException {
        Random generator = new Random();
        byte[] temp = new byte[]{0};
        boolean inRun = false;
        long runLength = 0;
        long best = 0;
        byte[] fileBytes = Arrays.copyOf(Files.readAllBytes(readPath), 255);

        for (int seed = 0; seed < Integer.MAX_VALUE; ++seed) {
            int readIndex = 0;
            generator.setSeed(seed);
            while (readIndex < fileBytes.length - runSize) {
                if (!inRun) {
                    byte min = Byte.MAX_VALUE;
                    byte max = Byte.MIN_VALUE;
                    for (int l = 0; l < runSize; ++l) {
                        int i = l + readIndex;
                        byte b = fileBytes[i];
                        min = (byte) Math.min(b, min);
                        max = (byte) Math.max(b, max);
                    }
                    offset = min;
                    mod = (byte) (max - min);
                    cursorIndex = readIndex;
                }
                byte read = fileBytes[readIndex];
                generator.nextBytes(temp);
                byte expected = (byte)(temp[0] + offset);
                if (mod != 0) {
                    expected = (byte) ((temp[0] % mod) + offset);
                }
                if (read == expected) {
                    if (!inRun) {
                        inRun = true;
                        runLength = 1;
                    } else {
                        runLength += 1;
                        if (runLength == runSize){
                            System.out.println("Complete run found In Seed: " + seed + " @ " + cursorIndex + " with offset " + offset +" and mod " + mod);
                            return true;
                        }
                    }
                } else {
                    inRun = false;
                    readIndex = cursorIndex;
                    generator.setSeed(seed);
                    best = Math.max(best, runLength);
                    if (runLength > 2) {
                        System.out.println("Incomplete Run found Length: " + runLength + " In Seed: " + seed + " @ " + cursorIndex + " with offset " + offset +" and mod " + mod + " Best: " + best);
                    }
                    runLength = 0;
                }

                readIndex += 1;
            }
        }

        return false;
    }
}
